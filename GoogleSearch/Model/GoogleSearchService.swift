//
//  GoogleSearchService.swift
//  GoogleSearch
//
//  Created by Ilya Sudnik on 6/22/16.
//  Copyright © 2016 Ilya Sudnik. All rights reserved.
//


import RxSwift

class GoogleSearchService {
    
    static var sharedManager = GoogleSearchService()
    
    private init() {}
    
    func getSuggestions(query: String) -> Observable<[String]> {
        
        if query.trim() == "" {
            
            return Observable.just([])
        }
        
        let url = NSURL(string: "http://google.com/complete/search?client=firefox&q=\(query.properQueryString)")!
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        return NSURLSession.sharedSession().rx_JSON(url)
            .observeOn(MainScheduler.instance)
            .map { array in
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                
                if let array = array as? [AnyObject] where array.count > 1 {
                    if let suggestions = array[1] as? [String] {
                        return suggestions
                    }
                }
                return []
        }
    }
    
}
