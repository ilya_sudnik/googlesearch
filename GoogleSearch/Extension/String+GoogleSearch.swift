//
//  String+GoogleSearch.swift
//  GoogleSearch
//
//  Created by Ilya Sudnik on 6/22/16.
//  Copyright © 2016 Ilya Sudnik. All rights reserved.
//

import Foundation

extension String {
    
    var properQueryString: String {
        
        return self.trim().replaceWhitespaces().escapeURL()
    }
    
    
    func trim() -> String {
        
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
    }
    
    func escapeURL() -> String {
        
        return self.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())!
    }
    
    func replaceWhitespaces() -> String {
        
        return self.stringByReplacingOccurrencesOfString(" ", withString: "+")
    }
    

    
    
}