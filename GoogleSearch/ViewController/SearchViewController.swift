//
//  SearchViewController.swift
//  GoogleSearch
//
//  Created by Ilya Sudnik on 6/22/16.
//  Copyright © 2016 Ilya Sudnik. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    let disposeBag = DisposeBag()
    
    var searchQuery: Observable<String> {
        return searchBar
            .rx_text
            .throttle(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
    }
    var googleSearchModel: GoogleSearchModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        googleSearchModel = GoogleSearchModel(searchQuery: searchQuery)

        googleSearchModel
            .getSuggestions()
            .bindTo(tableView.rx_itemsWithCellFactory) { (tableView, row, text) in
                
                let cell = tableView.dequeueReusableCellWithIdentifier("searchCell", forIndexPath: NSIndexPath(forRow: row, inSection: 0)) as UITableViewCell
                cell.textLabel?.text = text
                
                return cell
            }
            .addDisposableTo(disposeBag)
        
        tableView
            .rx_itemSelected
            .subscribeNext { [unowned self] indexPath in
                self.view.endEditing(true)
            }
            .addDisposableTo(disposeBag)
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
}
