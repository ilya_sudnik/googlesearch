//
//  GoogleSearchModel.swift
//  GoogleSearch
//
//  Created by Ilya Sudnik on 6/22/16.
//  Copyright © 2016 Ilya Sudnik. All rights reserved.
//

import Foundation
import RxSwift

struct GoogleSearchModel {
    
    let searchQuery: Observable<String>
    
    func getSuggestions() -> Observable<[String]> {
        return searchQuery
            .observeOn(MainScheduler.instance)
            .flatMapLatest { query -> Observable<[String]> in
                 GoogleSearchService.sharedManager.getSuggestions(query)
            }
    }
}